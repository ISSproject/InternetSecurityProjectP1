﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace testtcp
{
    public class CServer
    {
        ServerDBDataContext dc = new ServerDBDataContext();
        Dictionary<string, bool> dic = new Dictionary<string, bool>();
        TcpListener server;
        TcpClient client;
        public void StartServer()
        {
            server = new TcpListener(IPAddress.Any, 9050);
            server.Start();
            Console.WriteLine("Waiting for Client...");

            while (true)
            {
                client = server.AcceptTcpClient();
                Console.WriteLine("Connected with a client");
                /*threads !!*/
                Thread ClientThread = new Thread(adapter);
                ClientThread.Start();
            }
        }

        public void adapter()
        {
            HandleRequest(client);
        }

        public void HandleRequest(TcpClient client)
        {
            NetworkStream stream = client.GetStream();
            StreamReader reader = new StreamReader(stream);
            StreamWriter writer = new StreamWriter(stream);
            /*check alive !!*/
            IPGlobalProperties ipProperties = IPGlobalProperties.GetIPGlobalProperties();
            TcpConnectionInformation[] tcpConnections = ipProperties.GetActiveTcpConnections().Where(x => x.LocalEndPoint.Equals(client.Client.LocalEndPoint) && x.RemoteEndPoint.Equals(client.Client.RemoteEndPoint)).ToArray();
            /**/
            String line = null;
            bool active = true;
            string username = "";
            string encryptiontype = "";
            while (active)
            {
                try
                {
                    line = reader.ReadLine();
                    if (line != null)
                    {
                        Console.WriteLine(line);
                        if (line.Equals("Upload"))
                        {
                            int i = Convert.ToInt32(reader.ReadLine());
                            string accessableClient = reader.ReadLine();
                            for (int j = 0; j < i; j++)
                            {
                                var fileName = reader.ReadLine();
                                encryptiontype = reader.ReadLine();
                                string receivedPath = @"C:\server\";
                                Console.WriteLine("File:  {0}   received & saved at path: {1}", fileName, receivedPath);
                                var newfile = new File()
                                {
                                    Name = fileName,
                                    AccessableClient = accessableClient,
                                    Encryption = encryptiontype,
                                    Owner = username
                                };
                                try
                                {
                                    var existfile = dc.Files.FirstOrDefault(a => a.Name.Equals(fileName));
                                    if (existfile == null)
                                    {
                                        dc.Files.InsertOnSubmit(newfile);
                                        dc.SubmitChanges();
                                    }
                                    else
                                    {
                                        existfile.AccessableClient = accessableClient;
                                        var newedit = new LogFile()
                                        {
                                            EditedBy = username,
                                            FileId = existfile.Id,
                                            EditDate = DateTime.Now
                                        };
                                        dc.LogFiles.InsertOnSubmit(newedit);
                                        dc.SubmitChanges();
                                    }
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine("Upload Error !!", e.Message);
                                    throw;
                                }
                            }
                        }
                        else if (line.Equals("Download"))
                        {
                            //here :D 
                            username = reader.ReadLine();
                            encryptiontype = reader.ReadLine();
                            string filesName = responseDownload(username);
                            writer.WriteLine(filesName);
                            writer.Flush();
                        }
                        else
                        {
                            int l = line.IndexOf("/");
                            username = line.Substring(0, l);
                            string pass = line.Substring(line.IndexOf("/") + 1);

                            var user = dc.Users.FirstOrDefault(a => a.UserName.Equals(username) && a.Password.Equals(pass));
                            if (user != null)
                            { //found the requested user!
                                if (dic.ContainsKey(username) && dic.ContainsValue(true))
                                {
                                    writer.WriteLine("Active");
                                    writer.Flush();
                                }
                                else
                                {
                                    writer.WriteLine("True");
                                    writer.Flush();
                                    // add to active dic
                                    dic.Add(username, true);
                                }
                            }
                            else
                            { //Not found the requested user!
                                writer.WriteLine("False");
                                writer.Flush();
                            }
                        }
                    }
                    /*Check Alive*/
                    if (tcpConnections != null && tcpConnections.Length > 0)
                    {
                        TcpState stateOfConnection = tcpConnections.First().State;
                        if (stateOfConnection == TcpState.Established)
                        {
                            // Connection is OK
                            active = true;
                        }
                        else
                        {
                            // No active tcp Connection to hostName:port
                            active = false;
                        }

                    }
                    /*Check Alive End*/
                }
                catch (Exception ex)
                {
                    client.Close();
                    //server.Stop();
                    active = false;
                    Console.WriteLine("Client with ID:  {0}  disconnected!", username);
                    Console.WriteLine(ex.Message);
                    dic[username] = false;
                }
            }
        }

        public string responseDownload(string id)
        {
            DirectoryInfo dinfo = new DirectoryInfo(@"C:\server\");
            FileInfo[] Files = dinfo.GetFiles("*.*");
            string allFile = "";
            int i = 0;
            foreach (FileInfo file in Files)
            {
                if (isAccess(id, file.Name))
                    allFile += ((i++ == 0) ? file.Name : "-" + file.Name);
            }
            return allFile;
        }

        public bool isAccess(string id, string filename)
        {
            var savedfile = dc.Files.FirstOrDefault(c => c.Name.Equals(filename)/*&& c.EncryptionAlgorithm.Equals(encryptiontype)*/);
            if (savedfile != null)
            {
                if (savedfile.AccessableClient.Contains(id) || savedfile.Owner.Equals(id))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
