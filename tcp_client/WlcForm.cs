﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tcp_client
{
    public partial class WlcForm : MetroForm
    {
        CClient CurrentClient = CClient.GetInstance();
        public WlcForm()
        {
            InitializeComponent();
        }

        public void WlcInit(string ID)
        {
            this.IDLabel.Text = ID;
            this.Show();
        }

        private void StartChatButton_Click(object sender, EventArgs e)
        {
            UploadForm listform = new UploadForm();
            listform.Show();
            //this.Close();
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            DownloadForm dForm = new DownloadForm();
            dForm.Show();
        }

        private void KeyGeneraterButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                string publicKeyPath = Path.Combine(fbd.SelectedPath, "publicKey.xml");
                string privateKeyPath = Path.Combine(fbd.SelectedPath, "privateKey.xml");

                const int keySize = 1024;
                string publicKey;
                string privateKey;
                RSASection.RSACryptModel.GenerateKeys(keySize, out publicKey, out privateKey);
                using (StreamWriter sw = System.IO.File.CreateText(publicKeyPath))
                {
                    sw.Write(publicKey);
                }
                using (StreamWriter sw = System.IO.File.CreateText(privateKeyPath))
                {
                    sw.Write(privateKey);
                }
                /*save public key in DB*/
                CurrentClient.databaseFilePut(publicKeyPath);
            }
        }

        private void RSAUploadButton_Click(object sender, EventArgs e)
        {
            var impForm = new RSASection.ImportKeyForm();
            impForm.Show();
        }

        private void metroButton2_Click(object sender, EventArgs e)
        {
            var privateForm = new RSASection.ImportPrivateKeyForm();
            privateForm.Show();
        }

        private void metroButton3_Click(object sender, EventArgs e)
        {
            var uForm = new AESSection.UploadForm();
            uForm.Show();
        }

        private void metroButton4_Click(object sender, EventArgs e)
        {
            var dForm = new AESSection.ImportPrivateKeyForm();
            dForm.Show();
        }
    }
}
