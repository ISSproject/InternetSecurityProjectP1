﻿namespace tcp_client
{
    partial class DownloadForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.PasscodeBox = new MetroFramework.Controls.MetroTextBox();
            this.DownloadButton = new MetroFramework.Controls.MetroTextBox.MetroTextButton();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.LogTextBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(23, 64);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(152, 19);
            this.metroLabel1.TabIndex = 1;
            this.metroLabel1.Text = "Select File To Download:";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(23, 329);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(138, 19);
            this.metroLabel2.TabIndex = 2;
            this.metroLabel2.Text = "Enter Passcode String:";
            // 
            // PasscodeBox
            // 
            // 
            // 
            // 
            this.PasscodeBox.CustomButton.Image = null;
            this.PasscodeBox.CustomButton.Location = new System.Drawing.Point(263, 1);
            this.PasscodeBox.CustomButton.Name = "";
            this.PasscodeBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.PasscodeBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.PasscodeBox.CustomButton.TabIndex = 1;
            this.PasscodeBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.PasscodeBox.CustomButton.UseSelectable = true;
            this.PasscodeBox.CustomButton.Visible = false;
            this.PasscodeBox.Lines = new string[0];
            this.PasscodeBox.Location = new System.Drawing.Point(23, 361);
            this.PasscodeBox.MaxLength = 16;
            this.PasscodeBox.Name = "PasscodeBox";
            this.PasscodeBox.PasswordChar = '\0';
            this.PasscodeBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.PasscodeBox.SelectedText = "";
            this.PasscodeBox.SelectionLength = 0;
            this.PasscodeBox.SelectionStart = 0;
            this.PasscodeBox.Size = new System.Drawing.Size(285, 23);
            this.PasscodeBox.TabIndex = 3;
            this.PasscodeBox.UseSelectable = true;
            this.PasscodeBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.PasscodeBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.PasscodeBox.TextChanged += new System.EventHandler(this.PasscodeBox_TextChanged);
            // 
            // DownloadButton
            // 
            this.DownloadButton.Enabled = false;
            this.DownloadButton.Image = null;
            this.DownloadButton.Location = new System.Drawing.Point(121, 410);
            this.DownloadButton.Name = "DownloadButton";
            this.DownloadButton.Size = new System.Drawing.Size(108, 32);
            this.DownloadButton.Style = MetroFramework.MetroColorStyle.Green;
            this.DownloadButton.TabIndex = 6;
            this.DownloadButton.Text = "Download";
            this.DownloadButton.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.DownloadButton.UseSelectable = true;
            this.DownloadButton.UseVisualStyleBackColor = true;
            this.DownloadButton.Click += new System.EventHandler(this.DownloadButton_Click);
            // 
            // listBox1
            // 
            this.listBox1.Font = new System.Drawing.Font("Tahoma", 14F);
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 23;
            this.listBox1.Location = new System.Drawing.Point(23, 86);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(285, 211);
            this.listBox1.TabIndex = 7;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel3.Location = new System.Drawing.Point(353, 86);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(82, 25);
            this.metroLabel3.TabIndex = 9;
            this.metroLabel3.Text = "File Log :";
            // 
            // LogTextBox
            // 
            this.LogTextBox.Location = new System.Drawing.Point(353, 114);
            this.LogTextBox.Name = "LogTextBox";
            this.LogTextBox.Size = new System.Drawing.Size(291, 270);
            this.LogTextBox.TabIndex = 10;
            this.LogTextBox.Text = "";
            // 
            // DownloadForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(667, 465);
            this.Controls.Add(this.LogTextBox);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.DownloadButton);
            this.Controls.Add(this.PasscodeBox);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Name = "DownloadForm";
            this.Text = "Download Form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox PasscodeBox;
        private MetroFramework.Controls.MetroTextBox.MetroTextButton DownloadButton;
        private System.Windows.Forms.ListBox listBox1;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private System.Windows.Forms.RichTextBox LogTextBox;
    }
}