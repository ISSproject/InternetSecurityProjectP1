﻿namespace tcp_client
{
    partial class PassCodeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroTextButton1 = new MetroFramework.Controls.MetroTextBox.MetroTextButton();
            this.PasscodeLabel = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // metroTextButton1
            // 
            this.metroTextButton1.Font = new System.Drawing.Font("Tahoma", 12F);
            this.metroTextButton1.Image = null;
            this.metroTextButton1.Location = new System.Drawing.Point(155, 125);
            this.metroTextButton1.Name = "metroTextButton1";
            this.metroTextButton1.Size = new System.Drawing.Size(154, 38);
            this.metroTextButton1.TabIndex = 0;
            this.metroTextButton1.Text = "Copy Code";
            this.metroTextButton1.UseSelectable = true;
            this.metroTextButton1.UseVisualStyleBackColor = true;
            this.metroTextButton1.Click += new System.EventHandler(this.metroTextButton1_Click);
            // 
            // PasscodeLabel
            // 
            this.PasscodeLabel.AutoSize = true;
            this.PasscodeLabel.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.PasscodeLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.PasscodeLabel.Location = new System.Drawing.Point(137, 89);
            this.PasscodeLabel.Name = "PasscodeLabel";
            this.PasscodeLabel.Size = new System.Drawing.Size(172, 25);
            this.PasscodeLabel.Style = MetroFramework.MetroColorStyle.Green;
            this.PasscodeLabel.TabIndex = 1;
            this.PasscodeLabel.Text = "1234567890123456";
            this.PasscodeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(23, 60);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(146, 19);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "Save the EncryptionKey:";
            // 
            // PassCodeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 186);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.PasscodeLabel);
            this.Controls.Add(this.metroTextButton1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PassCodeForm";
            this.Text = "Upload Successful";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox.MetroTextButton metroTextButton1;
        private MetroFramework.Controls.MetroLabel PasscodeLabel;
        private MetroFramework.Controls.MetroLabel metroLabel1;
    }
}