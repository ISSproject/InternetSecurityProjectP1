﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tcp_client
{
    public partial class DownloadForm : MetroForm
    {
        CClient CurrentClient = CClient.GetInstance();
        DataClasses1DataContext dc = new DataClasses1DataContext();
        string allFiles = "";
        public DownloadForm()
        {
            InitializeComponent();
            //listBox1.SelectionMode = SelectionMode.MultiSimple;
            allFiles = CurrentClient.reqDownload("Rijndael");
            if (allFiles != null && allFiles != "")
            {
                string[] all = allFiles.Split('-');
                var items = listBox1.Items;
                foreach (string f in all)
                    items.Add(f);
            }
            /**/
            // Set the Multiline property to true.
            LogTextBox.Multiline = true;
            // Add vertical scroll bars to the TextBox control.
            LogTextBox.ScrollBars = RichTextBoxScrollBars.Vertical;
            LogTextBox.ReadOnly = true;
            LogTextBox.WordWrap = true;

        }

        private void PasscodeBox_TextChanged(object sender, EventArgs e)
        {
            if (PasscodeBox.Text.Length == 16)
            {
                this.DownloadButton.Enabled = true;
            }
        }

        private void DownloadButton_Click(object sender, EventArgs e)
        {
            var selectedItems = new ListBox.SelectedObjectCollection(listBox1);
            selectedItems = listBox1.SelectedItems;
            List<string> dfiles = new List<string>();
            foreach (object itemChecked in selectedItems)
            {
                dfiles.Add(itemChecked.ToString());
            }
            if (dfiles.Count != 0)
            {
                CurrentClient.downloadFile(dfiles, PasscodeBox.Text);
                DownloadSucceedForm ds = new DownloadSucceedForm();
                ds.Show();
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var file = dc.Files.FirstOrDefault(c => c.Name.Equals(listBox1.SelectedItem));
            if (file != null)
            {
                var filelog = dc.LogFiles.Where(c => c.FileId.Equals(file.Id)).ToList();
                LogTextBox.Text = "\t\tCreated By:  " + file.Owner.ToString() + "\n";
                foreach (var item in filelog)
                {
                    LogTextBox.Text += "Updated By:  " + item.EditedBy.ToString() + "\n";
                    LogTextBox.Text += "At:  " + item.EditDate.ToString() + "\n";
                    LogTextBox.Text += "-------\n";
                }
            }
            else
                LogTextBox.Text = "Error DataBase Not sycronices Yet!!";
        }
    }
}
