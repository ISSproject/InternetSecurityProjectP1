﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tcp_client
{
    public partial class PassCodeForm : MetroForm
    {
        public PassCodeForm()
        {
            InitializeComponent();
        }

        public void SetCode(string Key)
        {
            PasscodeLabel.Text = Key;
        }
        private void metroTextButton1_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(PasscodeLabel.Text);
            this.Hide();
        }
    }
}
