﻿using MetroFramework.Forms;
using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace tcp_client
{
    public partial class LoginForm : MetroForm
    {
        public static LoginForm curr = null;

        public CClient client;
        public LoginForm()
        {
            InitializeComponent();
            curr = this;
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            Regex pattern = new Regex("[0-9a-zA-Z]{6,}");
            if (pattern.IsMatch(PassBox.Text))
            {
                //MessageBox.Show("OK");
                client = CClient.GetInstance();
                client.Init(this.IDBox.Text.ToString(), this.PassBox.Text.ToString());
            }
            else
            {
                MessageBox.Show("Password Minimum 6 characters required");
            }
            
            
        }


        // Check for Enter input!
        private void CheckKeys(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                LoginButton_Click(sender, e);
                e.Handled = true;
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            RegisterForm regform = new RegisterForm();
            regform.Show();
            //this.Close();
            this.Hide();
        }

       
    }
}
