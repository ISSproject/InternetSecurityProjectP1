﻿namespace tcp_client
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginForm));
            this.IDLabel = new System.Windows.Forms.Label();
            this.IDBox = new System.Windows.Forms.TextBox();
            this.LoginButton = new MetroFramework.Controls.MetroButton();
            this.ExistErrorLabel = new System.Windows.Forms.Label();
            this.PassBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.LoginErrorLabel = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // IDLabel
            // 
            this.IDLabel.AutoSize = true;
            this.IDLabel.Location = new System.Drawing.Point(20, 78);
            this.IDLabel.Name = "IDLabel";
            this.IDLabel.Size = new System.Drawing.Size(110, 13);
            this.IDLabel.TabIndex = 0;
            this.IDLabel.Text = "Please Enter Your ID:";
            // 
            // IDBox
            // 
            this.IDBox.Location = new System.Drawing.Point(23, 103);
            this.IDBox.Name = "IDBox";
            this.IDBox.Size = new System.Drawing.Size(221, 20);
            this.IDBox.TabIndex = 1;
            // 
            // LoginButton
            // 
            this.LoginButton.Location = new System.Drawing.Point(72, 248);
            this.LoginButton.Name = "LoginButton";
            this.LoginButton.Size = new System.Drawing.Size(118, 34);
            this.LoginButton.TabIndex = 2;
            this.LoginButton.Text = "Login";
            this.LoginButton.UseSelectable = true;
            this.LoginButton.Click += new System.EventHandler(this.LoginButton_Click);
            // 
            // ExistErrorLabel
            // 
            this.ExistErrorLabel.AutoSize = true;
            this.ExistErrorLabel.ForeColor = System.Drawing.Color.Red;
            this.ExistErrorLabel.Location = new System.Drawing.Point(10, 232);
            this.ExistErrorLabel.Name = "ExistErrorLabel";
            this.ExistErrorLabel.Size = new System.Drawing.Size(257, 13);
            this.ExistErrorLabel.TabIndex = 3;
            this.ExistErrorLabel.Text = "This ID is already in use, Please choice another one.";
            this.ExistErrorLabel.Visible = false;
            // 
            // PassBox
            // 
            this.PassBox.Location = new System.Drawing.Point(23, 170);
            this.PassBox.Name = "PassBox";
            this.PassBox.PasswordChar = '*';
            this.PassBox.Size = new System.Drawing.Size(221, 20);
            this.PassBox.TabIndex = 4;
            this.PassBox.UseSystemPasswordChar = true;
            this.PassBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeys);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 142);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Please Enter Your Password:";
            // 
            // LoginErrorLabel
            // 
            this.LoginErrorLabel.AutoSize = true;
            this.LoginErrorLabel.ForeColor = System.Drawing.Color.Red;
            this.LoginErrorLabel.Location = new System.Drawing.Point(20, 203);
            this.LoginErrorLabel.Name = "LoginErrorLabel";
            this.LoginErrorLabel.Size = new System.Drawing.Size(189, 13);
            this.LoginErrorLabel.TabIndex = 6;
            this.LoginErrorLabel.Text = "Please Make Sure From Login Details!!";
            this.LoginErrorLabel.Visible = false;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Font = new System.Drawing.Font("Tahoma", 11F);
            this.linkLabel1.Location = new System.Drawing.Point(113, 298);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(52, 18);
            this.linkLabel1.TabIndex = 7;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "SignUp";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label2.Location = new System.Drawing.Point(23, 298);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 17);
            this.label2.TabIndex = 8;
            this.label2.Text = "New User ?";
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 343);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.LoginErrorLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PassBox);
            this.Controls.Add(this.ExistErrorLabel);
            this.Controls.Add(this.LoginButton);
            this.Controls.Add(this.IDBox);
            this.Controls.Add(this.IDLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LoginForm";
            this.Text = "Login";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label IDLabel;
        private MetroFramework.Controls.MetroButton LoginButton;
        public System.Windows.Forms.TextBox IDBox;
        public System.Windows.Forms.Label ExistErrorLabel;
        private System.Windows.Forms.TextBox PassBox;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label LoginErrorLabel;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label2;
    }
}

