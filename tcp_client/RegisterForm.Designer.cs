﻿namespace tcp_client
{
    partial class RegisterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.PassBox = new System.Windows.Forms.TextBox();
            this.ExistErrorLabel = new System.Windows.Forms.Label();
            this.LoginButton = new MetroFramework.Controls.MetroButton();
            this.IDBox = new System.Windows.Forms.TextBox();
            this.IDLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.PassBoxCheck = new System.Windows.Forms.TextBox();
            this.MatchLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label2.Location = new System.Drawing.Point(7, 280);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(144, 17);
            this.label2.TabIndex = 16;
            this.label2.Text = "Already got Account ?";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Font = new System.Drawing.Font("Tahoma", 11F);
            this.linkLabel1.Location = new System.Drawing.Point(157, 278);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(41, 18);
            this.linkLabel1.TabIndex = 15;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Login";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 108);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Please Enter Your Password:";
            // 
            // PassBox
            // 
            this.PassBox.Location = new System.Drawing.Point(26, 124);
            this.PassBox.Name = "PassBox";
            this.PassBox.PasswordChar = '*';
            this.PassBox.Size = new System.Drawing.Size(221, 20);
            this.PassBox.TabIndex = 13;
            this.PassBox.UseSystemPasswordChar = true;
            // 
            // ExistErrorLabel
            // 
            this.ExistErrorLabel.AutoSize = true;
            this.ExistErrorLabel.ForeColor = System.Drawing.Color.Red;
            this.ExistErrorLabel.Location = new System.Drawing.Point(23, 197);
            this.ExistErrorLabel.Name = "ExistErrorLabel";
            this.ExistErrorLabel.Size = new System.Drawing.Size(257, 13);
            this.ExistErrorLabel.TabIndex = 12;
            this.ExistErrorLabel.Text = "This ID is already in use, Please choice another one.";
            this.ExistErrorLabel.Visible = false;
            // 
            // LoginButton
            // 
            this.LoginButton.Location = new System.Drawing.Point(80, 235);
            this.LoginButton.Name = "LoginButton";
            this.LoginButton.Size = new System.Drawing.Size(105, 30);
            this.LoginButton.TabIndex = 11;
            this.LoginButton.Text = "SignUp";
            this.LoginButton.UseSelectable = true;
            this.LoginButton.Click += new System.EventHandler(this.LoginButton_Click);
            // 
            // IDBox
            // 
            this.IDBox.Location = new System.Drawing.Point(26, 76);
            this.IDBox.Name = "IDBox";
            this.IDBox.Size = new System.Drawing.Size(221, 20);
            this.IDBox.TabIndex = 10;
            // 
            // IDLabel
            // 
            this.IDLabel.AutoSize = true;
            this.IDLabel.Location = new System.Drawing.Point(23, 60);
            this.IDLabel.Name = "IDLabel";
            this.IDLabel.Size = new System.Drawing.Size(110, 13);
            this.IDLabel.TabIndex = 9;
            this.IDLabel.Text = "Please Enter Your ID:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 158);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(162, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Please Re-Enter Your Password:";
            // 
            // PassBoxCheck
            // 
            this.PassBoxCheck.Location = new System.Drawing.Point(26, 174);
            this.PassBoxCheck.Name = "PassBoxCheck";
            this.PassBoxCheck.PasswordChar = '*';
            this.PassBoxCheck.Size = new System.Drawing.Size(221, 20);
            this.PassBoxCheck.TabIndex = 17;
            this.PassBoxCheck.UseSystemPasswordChar = true;
            // 
            // MatchLabel
            // 
            this.MatchLabel.AutoSize = true;
            this.MatchLabel.ForeColor = System.Drawing.Color.Red;
            this.MatchLabel.Location = new System.Drawing.Point(23, 210);
            this.MatchLabel.Name = "MatchLabel";
            this.MatchLabel.Size = new System.Drawing.Size(186, 13);
            this.MatchLabel.TabIndex = 19;
            this.MatchLabel.Text = "Password and Confirem Dont Match!!";
            this.MatchLabel.Visible = false;
            // 
            // RegisterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 343);
            this.Controls.Add(this.MatchLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.PassBoxCheck);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PassBox);
            this.Controls.Add(this.ExistErrorLabel);
            this.Controls.Add(this.LoginButton);
            this.Controls.Add(this.IDBox);
            this.Controls.Add(this.IDLabel);
            this.Name = "RegisterForm";
            this.Text = "RegisterForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox PassBox;
        public System.Windows.Forms.Label ExistErrorLabel;
        private MetroFramework.Controls.MetroButton LoginButton;
        public System.Windows.Forms.TextBox IDBox;
        private System.Windows.Forms.Label IDLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox PassBoxCheck;
        public System.Windows.Forms.Label MatchLabel;
    }
}