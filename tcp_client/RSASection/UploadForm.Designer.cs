﻿namespace tcp_client.RSASection
{
    partial class UploadForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LoggedAsLabel = new MetroFramework.Controls.MetroLabel();
            this.LoggedUserLabel = new MetroFramework.Controls.MetroLink();
            this.metroUploadButton = new MetroFramework.Controls.MetroTextBox.MetroTextButton();
            this.ClearButton = new MetroFramework.Controls.MetroButton();
            this.Browsebutton = new MetroFramework.Controls.MetroButton();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // LoggedAsLabel
            // 
            this.LoggedAsLabel.AutoSize = true;
            this.LoggedAsLabel.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.LoggedAsLabel.Location = new System.Drawing.Point(393, 446);
            this.LoggedAsLabel.Name = "LoggedAsLabel";
            this.LoggedAsLabel.Size = new System.Drawing.Size(77, 19);
            this.LoggedAsLabel.TabIndex = 18;
            this.LoggedAsLabel.Text = "Logged As:";
            // 
            // LoggedUserLabel
            // 
            this.LoggedUserLabel.Location = new System.Drawing.Point(465, 446);
            this.LoggedUserLabel.Name = "LoggedUserLabel";
            this.LoggedUserLabel.Size = new System.Drawing.Size(75, 23);
            this.LoggedUserLabel.TabIndex = 17;
            this.LoggedUserLabel.Text = "UserID";
            this.LoggedUserLabel.UseSelectable = true;
            // 
            // metroUploadButton
            // 
            this.metroUploadButton.Image = null;
            this.metroUploadButton.Location = new System.Drawing.Point(73, 410);
            this.metroUploadButton.Name = "metroUploadButton";
            this.metroUploadButton.Size = new System.Drawing.Size(247, 50);
            this.metroUploadButton.TabIndex = 15;
            this.metroUploadButton.Text = "Start Upload";
            this.metroUploadButton.UseSelectable = true;
            this.metroUploadButton.UseVisualStyleBackColor = true;
            this.metroUploadButton.Click += new System.EventHandler(this.metroUploadButton1_Click);
            // 
            // ClearButton
            // 
            this.ClearButton.Location = new System.Drawing.Point(274, 93);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(118, 35);
            this.ClearButton.TabIndex = 13;
            this.ClearButton.Text = "Clear List";
            this.ClearButton.UseSelectable = true;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // Browsebutton
            // 
            this.Browsebutton.Location = new System.Drawing.Point(23, 93);
            this.Browsebutton.Name = "Browsebutton";
            this.Browsebutton.Size = new System.Drawing.Size(118, 35);
            this.Browsebutton.TabIndex = 12;
            this.Browsebutton.Text = "Browse Files";
            this.Browsebutton.UseSelectable = true;
            this.Browsebutton.Click += new System.EventHandler(this.Browsebutton_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(23, 134);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(369, 264);
            this.listBox1.TabIndex = 11;
            // 
            // UploadForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(551, 481);
            this.Controls.Add(this.LoggedAsLabel);
            this.Controls.Add(this.LoggedUserLabel);
            this.Controls.Add(this.metroUploadButton);
            this.Controls.Add(this.ClearButton);
            this.Controls.Add(this.Browsebutton);
            this.Controls.Add(this.listBox1);
            this.Name = "UploadForm";
            this.Text = "Upload Form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel LoggedAsLabel;
        private MetroFramework.Controls.MetroLink LoggedUserLabel;
        private MetroFramework.Controls.MetroTextBox.MetroTextButton metroUploadButton;
        private MetroFramework.Controls.MetroButton ClearButton;
        private MetroFramework.Controls.MetroButton Browsebutton;
        private System.Windows.Forms.ListBox listBox1;
    }
}