﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tcp_client.RSASection
{
    public partial class ImportPrivateKeyForm : MetroForm
    {
        public ImportPrivateKeyForm()
        {
            InitializeComponent();
        }
        private void metroButton2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
        private void ImportButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog FBD = new OpenFileDialog();
            FBD.InitialDirectory = @"C:\download\";
            FBD.Filter = "All files (*.*)|*.*";
            FBD.FilterIndex = 2;
            FBD.RestoreDirectory = true;
            if (FBD.ShowDialog() == DialogResult.OK)
            {
                /*show key Content*/
                using (StreamReader sr = System.IO.File.OpenText(FBD.FileName))
                    this.KeyTextBox.Text = sr.ReadToEnd();
            }
        }
        private void metroOKButton_Click(object sender, EventArgs e)
        {
            var dwform = new RSASection.DownloadForm();
            dwform.privateKey = KeyTextBox.Text;
            dwform.Show();
            this.Hide();
        }


    }
}
