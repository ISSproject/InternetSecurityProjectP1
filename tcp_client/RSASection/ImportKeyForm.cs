﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tcp_client.RSASection
{
    public partial class ImportKeyForm : MetroForm
    {
        CClient CurrentClient = CClient.GetInstance();
        DataClasses1DataContext dc = new DataClasses1DataContext();
        string keyPath = @"C:\server\Keys\";
        string FileName = @"\publicKey.xml";
        string selectedItem = "";
        public ImportKeyForm()
        {
            InitializeComponent();
            var users = dc.Users.ToList();
            foreach (var item in users)
            {
                if (item.UserName != CurrentClient.ID)
                    metroComboBox1.Items.Add(item.UserName);
            }
        }

        private void metroButton2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void ImportButton_Click(object sender, EventArgs e)
        {
            selectedItem = metroComboBox1.SelectedItem.ToString();

            var selecteduser = dc.Users.FirstOrDefault(c => c.UserName.Equals(selectedItem));
            if (selecteduser != null && selecteduser.PublicKey != null)
            {
                CurrentClient.databaseFileRead(selectedItem, keyPath);
                /*show key Content*/
                using (StreamReader sr = System.IO.File.OpenText(keyPath + selectedItem + FileName))
                {
                    this.KeyTextBox.Text = sr.ReadToEnd();
                }
            }

        }

        private void metroOKButton_Click(object sender, EventArgs e)
        {

            UploadForm upform = new UploadForm();
            upform.selecteduser = selectedItem;
            upform.publicKey = KeyTextBox.Text;
            upform.Show();
            this.Hide();
        }
    }
}
