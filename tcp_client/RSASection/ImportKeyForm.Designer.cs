﻿namespace tcp_client.RSASection
{
    partial class ImportKeyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ImportButton = new MetroFramework.Controls.MetroButton();
            this.metroButton2 = new MetroFramework.Controls.MetroButton();
            this.metroOKButton = new MetroFramework.Controls.MetroButton();
            this.KeyTextBox = new MetroFramework.Controls.MetroTextBox();
            this.metroComboBox1 = new MetroFramework.Controls.MetroComboBox();
            this.SuspendLayout();
            // 
            // ImportButton
            // 
            this.ImportButton.Location = new System.Drawing.Point(23, 319);
            this.ImportButton.Name = "ImportButton";
            this.ImportButton.Size = new System.Drawing.Size(185, 32);
            this.ImportButton.TabIndex = 0;
            this.ImportButton.Text = "Import public key from XML file";
            this.ImportButton.UseSelectable = true;
            this.ImportButton.Click += new System.EventHandler(this.ImportButton_Click);
            // 
            // metroButton2
            // 
            this.metroButton2.Location = new System.Drawing.Point(448, 319);
            this.metroButton2.Name = "metroButton2";
            this.metroButton2.Size = new System.Drawing.Size(75, 32);
            this.metroButton2.TabIndex = 1;
            this.metroButton2.Text = "Cancel";
            this.metroButton2.UseSelectable = true;
            this.metroButton2.Click += new System.EventHandler(this.metroButton2_Click);
            // 
            // metroOKButton
            // 
            this.metroOKButton.Location = new System.Drawing.Point(346, 319);
            this.metroOKButton.Name = "metroOKButton";
            this.metroOKButton.Size = new System.Drawing.Size(75, 32);
            this.metroOKButton.TabIndex = 2;
            this.metroOKButton.Text = "OK";
            this.metroOKButton.UseSelectable = true;
            this.metroOKButton.Click += new System.EventHandler(this.metroOKButton_Click);
            // 
            // KeyTextBox
            // 
            // 
            // 
            // 
            this.KeyTextBox.CustomButton.Image = null;
            this.KeyTextBox.CustomButton.Location = new System.Drawing.Point(340, 1);
            this.KeyTextBox.CustomButton.Name = "";
            this.KeyTextBox.CustomButton.Size = new System.Drawing.Size(159, 159);
            this.KeyTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.KeyTextBox.CustomButton.TabIndex = 1;
            this.KeyTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.KeyTextBox.CustomButton.UseSelectable = true;
            this.KeyTextBox.CustomButton.Visible = false;
            this.KeyTextBox.Lines = new string[0];
            this.KeyTextBox.Location = new System.Drawing.Point(23, 74);
            this.KeyTextBox.MaxLength = 32767;
            this.KeyTextBox.Multiline = true;
            this.KeyTextBox.Name = "KeyTextBox";
            this.KeyTextBox.PasswordChar = '\0';
            this.KeyTextBox.ReadOnly = true;
            this.KeyTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.KeyTextBox.SelectedText = "";
            this.KeyTextBox.SelectionLength = 0;
            this.KeyTextBox.SelectionStart = 0;
            this.KeyTextBox.Size = new System.Drawing.Size(500, 161);
            this.KeyTextBox.TabIndex = 3;
            this.KeyTextBox.UseSelectable = true;
            this.KeyTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.KeyTextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroComboBox1
            // 
            this.metroComboBox1.FormattingEnabled = true;
            this.metroComboBox1.ItemHeight = 23;
            this.metroComboBox1.Location = new System.Drawing.Point(23, 256);
            this.metroComboBox1.Name = "metroComboBox1";
            this.metroComboBox1.Size = new System.Drawing.Size(121, 29);
            this.metroComboBox1.TabIndex = 4;
            this.metroComboBox1.UseSelectable = true;
            // 
            // ImportKeyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 372);
            this.Controls.Add(this.metroComboBox1);
            this.Controls.Add(this.KeyTextBox);
            this.Controls.Add(this.metroOKButton);
            this.Controls.Add(this.metroButton2);
            this.Controls.Add(this.ImportButton);
            this.MaximizeBox = false;
            this.Name = "ImportKeyForm";
            this.Text = "Public key:";
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroButton ImportButton;
        private MetroFramework.Controls.MetroButton metroButton2;
        private MetroFramework.Controls.MetroButton metroOKButton;
        private MetroFramework.Controls.MetroTextBox KeyTextBox;
        private MetroFramework.Controls.MetroComboBox metroComboBox1;
    }
}