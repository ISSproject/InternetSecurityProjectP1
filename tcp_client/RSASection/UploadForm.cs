﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tcp_client.RSASection
{
    public partial class UploadForm : MetroForm
    {
        CClient CurrentClient = CClient.GetInstance();
        DataClasses1DataContext dc = new DataClasses1DataContext();
        public string selecteduser;
        public string publicKey;
        private EventWaitHandle freeEvent = new EventWaitHandle(true, EventResetMode.ManualReset);
        public UploadForm()
        {
            InitializeComponent();
            listBox1.SelectionMode = SelectionMode.MultiSimple;

            this.LoggedUserLabel.Text = CurrentClient.ID.ToString();
        }

        private void Browsebutton_Click(object sender, EventArgs e)
        {
            OpenFileDialog FBD = new OpenFileDialog();
            FBD.InitialDirectory = @"C:\upload\";
            FBD.Filter = "All files (*.*)|*.*";
            FBD.FilterIndex = 2;
            FBD.RestoreDirectory = true;
            FBD.Multiselect = true;
            if (FBD.ShowDialog() == DialogResult.OK)
            {
                //listBox1.Items.Clear();
                string[] files = FBD.FileNames;
                foreach (string file in files)
                {
                    listBox1.Items.Add(/*Path.GetFileName*/(file));
                }
            }
        }
        private void ClearButton_Click(object sender, EventArgs e)
        {
            var selectedItems = new ListBox.SelectedObjectCollection(listBox1);
            selectedItems = listBox1.SelectedItems;

            if (listBox1.SelectedIndex != -1)
            {
                for (int i = selectedItems.Count - 1; i >= 0; i--)
                    listBox1.Items.Remove(selectedItems[i]);
            }
        }
        private void metroUploadButton1_Click(object sender, EventArgs e)
        {
            if (!this.freeEvent.WaitOne(0))
            {
                MessageBox.Show("Please Wait Until Backend encryption Finish :)");
                return;
            }
            string accessableClient = selecteduser;
            CurrentClient.reqUpload(listBox1.Items.Count, accessableClient);

            foreach (string item in listBox1.Items)
            {
                var t = Task.Factory.StartNew(() =>
                {
                    freeEvent.Reset();
                    /*string msg =*/
                    CurrentClient.uploadFileWithRSA(item, publicKey);
                    freeEvent.Set();
                });
            }
            OperationSucceedForm opform = new OperationSucceedForm();
            opform.Text = "Upload Succeed";
            opform.Show();
        }
    }
}
