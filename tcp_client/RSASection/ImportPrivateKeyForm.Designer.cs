﻿namespace tcp_client.RSASection
{
    partial class ImportPrivateKeyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.KeyTextBox = new MetroFramework.Controls.MetroTextBox();
            this.metroOKButton = new MetroFramework.Controls.MetroButton();
            this.metroButton2 = new MetroFramework.Controls.MetroButton();
            this.ImportButton = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // KeyTextBox
            // 
            // 
            // 
            // 
            this.KeyTextBox.CustomButton.Image = null;
            this.KeyTextBox.CustomButton.Location = new System.Drawing.Point(340, 1);
            this.KeyTextBox.CustomButton.Name = "";
            this.KeyTextBox.CustomButton.Size = new System.Drawing.Size(159, 159);
            this.KeyTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.KeyTextBox.CustomButton.TabIndex = 1;
            this.KeyTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.KeyTextBox.CustomButton.UseSelectable = true;
            this.KeyTextBox.CustomButton.Visible = false;
            this.KeyTextBox.Lines = new string[0];
            this.KeyTextBox.Location = new System.Drawing.Point(23, 73);
            this.KeyTextBox.MaxLength = 32767;
            this.KeyTextBox.Multiline = true;
            this.KeyTextBox.Name = "KeyTextBox";
            this.KeyTextBox.PasswordChar = '\0';
            this.KeyTextBox.ReadOnly = true;
            this.KeyTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.KeyTextBox.SelectedText = "";
            this.KeyTextBox.SelectionLength = 0;
            this.KeyTextBox.SelectionStart = 0;
            this.KeyTextBox.Size = new System.Drawing.Size(500, 161);
            this.KeyTextBox.TabIndex = 8;
            this.KeyTextBox.UseSelectable = true;
            this.KeyTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.KeyTextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroOKButton
            // 
            this.metroOKButton.Location = new System.Drawing.Point(345, 253);
            this.metroOKButton.Name = "metroOKButton";
            this.metroOKButton.Size = new System.Drawing.Size(75, 32);
            this.metroOKButton.TabIndex = 7;
            this.metroOKButton.Text = "OK";
            this.metroOKButton.UseSelectable = true;
            this.metroOKButton.Click += new System.EventHandler(this.metroOKButton_Click);
            // 
            // metroButton2
            // 
            this.metroButton2.Location = new System.Drawing.Point(447, 253);
            this.metroButton2.Name = "metroButton2";
            this.metroButton2.Size = new System.Drawing.Size(75, 32);
            this.metroButton2.TabIndex = 6;
            this.metroButton2.Text = "Cancel";
            this.metroButton2.UseSelectable = true;
            this.metroButton2.Click += new System.EventHandler(this.metroButton2_Click);
            // 
            // ImportButton
            // 
            this.ImportButton.Location = new System.Drawing.Point(22, 253);
            this.ImportButton.Name = "ImportButton";
            this.ImportButton.Size = new System.Drawing.Size(198, 32);
            this.ImportButton.TabIndex = 5;
            this.ImportButton.Text = "Import private key from XML file";
            this.ImportButton.UseSelectable = true;
            this.ImportButton.Click += new System.EventHandler(this.ImportButton_Click);
            // 
            // ImportPrivateKeyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 314);
            this.Controls.Add(this.KeyTextBox);
            this.Controls.Add(this.metroOKButton);
            this.Controls.Add(this.metroButton2);
            this.Controls.Add(this.ImportButton);
            this.Name = "ImportPrivateKeyForm";
            this.Text = "Private Key:";
            this.ResumeLayout(false);

        }

        #endregion
        private MetroFramework.Controls.MetroTextBox KeyTextBox;
        private MetroFramework.Controls.MetroButton metroOKButton;
        private MetroFramework.Controls.MetroButton metroButton2;
        private MetroFramework.Controls.MetroButton ImportButton;
    }
}