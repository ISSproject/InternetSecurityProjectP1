﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tcp_client.RSASection
{
    public partial class OperationSucceedForm : MetroForm
    {
        public OperationSucceedForm()
        {
            InitializeComponent();
        }

        private void metroTextButton1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
