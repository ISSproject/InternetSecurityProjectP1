﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Xml.XPath;

namespace tcp_client
{
    public class CClient
    { //Singleton Class
        public string ID;
        string Pass;
        TcpClient client;
        NetworkStream stream;
        StreamReader reader;
        StreamWriter writer;
        private static readonly CClient _instance = new CClient();
        DataClasses1DataContext dc = new DataClasses1DataContext();
        string tempNewPath = "";
        private CClient() { }
        public static CClient GetInstance()
        {
            return _instance;
        }

        public void Init(string ID, string pass)
        {
            /**/
            client = new TcpClient("localhost", 9050);
            stream = client.GetStream();
            reader = new StreamReader(stream);
            writer = new StreamWriter(stream);

            this.ID = ID;
            this.Pass = CryptModel.Encrypt(pass);
            string con = RegisterOnServer(ID, Pass);
            switch (con)
            {
                case "True":
                    {
                        WlcForm wlcForm = new WlcForm();
                        //wlcForm.CurrentClient = Form1.curr.client;
                        wlcForm.WlcInit(ID);
                    }
                    break;
                case "Active":
                    LoginForm.curr.ExistErrorLabel.Visible = true;
                    break;
                default:
                    LoginForm.curr.LoginErrorLabel.Visible = true;
                    break;
            }
        }
        public void Register(string ID, string pass)
        {
            this.ID = ID;
            this.Pass = CryptModel.Encrypt(pass);
            var newuser = new User()
            {
                Password = Pass,
                UserName = ID
            };
            var user = dc.Users.FirstOrDefault(a => a.UserName.Equals(ID));
            if (user == null)
            {
                dc.Users.InsertOnSubmit(newuser);
                dc.SubmitChanges();

                LoginForm form1 = new LoginForm();
                form1.Show();
                RegisterForm.rform.Close();
            }
            else
            {
                RegisterForm.rform.ExistErrorLabel.Visible = true;
            }
        }

        internal void uploadFile(string dir, string key)
        {
            string name = Path.GetFileName(dir);
            SendId(name);
            SendId("Rijndael");
            RijndaelCryptModel.EncryptFile(dir, @"C:\server\" + name, key);
        }
        internal void downloadFile(List<string> dfiles, string key)
        {
            foreach (var file in dfiles)
            {
                RijndaelCryptModel.DecryptFile(@"C:\server\" + file, @"C:\download\" + file, key);
            }
        }
        internal void downloadFileWithRSA(List<string> dfiles, string Privatekey)
        {
            string RSAKey = Privatekey;
            string encryptedFilePath = @"C:\server\";
            string decryptFilePath = @"C:\download\";
            foreach (var file in dfiles)
            {
                string manifestFile = MakePath(encryptedFilePath + file, ".manifest.xml");
                XDocument doc = XDocument.Load(manifestFile);
                XElement aesKeyElement = doc.Root.XPathSelectElement("./DataEncryption/AESEncryptedKeyValue/Key");
                byte[] aesKey = RSASection.RSACryptModel.RSADescryptBytes(Convert.FromBase64String(aesKeyElement.Value), RSAKey);
                XElement aesIvElement = doc.Root.XPathSelectElement("./DataEncryption/AESEncryptedKeyValue/IV");
                byte[] aesIv = RSASection.RSACryptModel.RSADescryptBytes(Convert.FromBase64String(aesIvElement.Value), RSAKey);


                RSASection.RSACryptModel.DecryptFile(decryptFilePath + file, encryptedFilePath + file, aesKey, aesIv);
            }
        }
        internal void uploadFileWithRSA(string dir, string RSAKey)
        {
            string name = Path.GetFileName(dir);
            SendId(name);
            SendId("RSA");
            string encryptedFilePath = @"C:\server\" + name;
            string manifestFilePath = MakePath(encryptedFilePath, ".manifest.xml");

            /*string s =*/
            RSASection.RSACryptModel.Encrypt(dir, encryptedFilePath, manifestFilePath, RSAKey);
            //return s;
        }
        internal void reqUpload(int count, string accessableClient)
        {
            SendId("Upload");
            SendId(count.ToString());
            SendId(accessableClient);
        }

        internal string reqDownload(string algorithm)
        {
            SendId("Download");
            SendId(ID);
            SendId(algorithm);
            string filesname = RecvId();
            return filesname;
        }
        public string RegisterOnServer(string ID, string pass)
        {
            string Req = ID + "/" + pass;
            SendId(Req);


            Req = RecvId();
            if (Req.CompareTo("True") == 0)
            { // Found !! :D
                return "True";
            }
            else if (Req.CompareTo("Active") == 0)
            {
                return "Active";
            }

            else
            { //Not Found !!
                return "False";
            }
        }
        private string RecvId()
        {
            string msg = reader.ReadLine();
            return msg;
        }
        private void SendId(string req)
        {
            writer.WriteLine(req);
            writer.Flush();
        }

        /// <summary>
        /// Save PublicKey To DataBase
        /// </summary>
        /// <param name="varFilePath"> PublicKey Path</param>
        public void databaseFilePut(string varFilePath)
        {
            byte[] file;
            using (var stream = new FileStream(varFilePath, FileMode.Open, FileAccess.Read))
            using (var reader = new BinaryReader(stream))
                file = reader.ReadBytes((int)stream.Length);

            var curUser = dc.Users.FirstOrDefault(e => e.UserName.Equals(ID));
            curUser.PublicKey = file;
            dc.SubmitChanges();
        }
        /// <summary>
        /// Read PublicKey From Database And Save it in New Path
        /// </summary>
        /// <param name="varID"></param>
        /// <param name="varPathToNewLocation"></param>
        public void databaseFileRead(string varID, string varPathToNewLocation)
        {
            var curUser = dc.Users.FirstOrDefault(e => e.UserName.Equals(varID));
            if (curUser != null)
            {
                /*create path for user key*/
                string subPath = varID; // your code goes here
                bool exists = Directory.Exists(varPathToNewLocation + subPath);
                if (!exists)
                    Directory.CreateDirectory(varPathToNewLocation + subPath);
                /*end path*/
                var Result = curUser.PublicKey.ToArray();
                using (var fs = new FileStream(varPathToNewLocation + subPath + "\\publicKey.xml", FileMode.Create, FileAccess.Write))
                    fs.Write(Result, 0, Result.Length);
                tempNewPath = varPathToNewLocation + subPath + "\\publicKey.xml";
            }
        }
        #region Used In AES
        internal void uploadFileWithAES(string dir, string key, string recivers)
        {
            string name = Path.GetFileName(dir);
            SendId(name);
            //SendId(Properties.Resources.AESAlgorithm);
            SendId("AES");
            string[] clients = recivers.Split('-');

            string encryptedFilePath = @"C:\server\" + name;
            RijndaelCryptModel.EncryptFile(dir, encryptedFilePath, key);
            databaseFilePut(encryptedFilePath, "FileContent");

            string tempPath = @"C:\server\Temp\";
            const int keySize = 1024;
            foreach (var client in clients)
            {
                string publicKey = "";
                databaseFileRead(client, tempPath);
                using (StreamReader sr = System.IO.File.OpenText(tempNewPath))
                {
                    publicKey = sr.ReadToEnd();
                }

                string encrypted = RSASection.RSACryptModel.Encrypt(key, keySize, publicKey);
                databaseFilePutkey(encrypted, name, client);
            }
        }
        internal void downloadFileWithAES(List<string> dfiles, string Privatekey)
        {
            const int keySize = 1024;
            string decryptFilePath = @"C:\download\";

            foreach (var file in dfiles)
            {
                string AESKey = databaseFileReadkey(ID, file).ToString();

                string decryptedKey = RSASection.RSACryptModel.Decrypt(AESKey, keySize, Privatekey);
                /*still need to retive file from DB*/
                //Console.WriteLine(decryptedKey);
                
                RijndaelCryptModel.DecryptFile(@"C:\server\" + file, decryptFilePath + file, decryptedKey);
            }
        }

        /// <summary>
        /// Save File Content To Database
        /// </summary>
        /// <param name="varFilePath"></param>
        /// <param name="reciver"></param>
        public void databaseFilePut(string varFilePath, string kind)
        {
            byte[] file;
            using (var stream = new FileStream(varFilePath, FileMode.Open, FileAccess.Read))
            using (var reader = new BinaryReader(stream))
                file = reader.ReadBytes((int)stream.Length);
            var dbfile = dc.Files.FirstOrDefault(e => e.Name.Equals(Path.GetFileName(varFilePath)));
            if (dbfile != null)
            {
                dbfile.FileContent = file;
                dc.SubmitChanges();
            }
            else
            {
                var opform = new RSASection.OperationSucceedForm();
                opform.Text = "Error Database Not Synchronize Yet !!";
                opform.Show();
            }
        }

        /// <summary>
        /// Save AES Encrypted Key
        /// </summary>
        /// <param name="varkey"></param>
        /// <param name="encryptedFileName"></param>
        /// <param name="client"></param>
        public void databaseFilePutkey(string varkey, string encryptedFileName, string client)
        {
            var dbfile = dc.Files.FirstOrDefault(e => e.Name.Equals(encryptedFileName));
            var newkey = new FileKey()
            {
                EncryptedAESKey = varkey,
                FileName = encryptedFileName,
                AccessableClient = client
            };
            dc.FileKeys.InsertOnSubmit(newkey);
            dc.SubmitChanges();
        }
        /// <summary>
        /// Read AES Encrypted Key From Database
        /// </summary>
        /// <param name="varID"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public string databaseFileReadkey(string varID, string fileName)
        {
            string Result = "";
            var dbkey = dc.FileKeys.FirstOrDefault(e => e.AccessableClient.Equals(varID)
            && e.FileName.Equals(fileName));
            if (dbkey != null)
            {
                Result = dbkey.EncryptedAESKey;
            }
            return Result;
        }

        #endregion
        private static string MakePath(string plainFilePath, string newSuffix)
        {
            string encryptedFileName = Path.GetFileNameWithoutExtension(plainFilePath) + newSuffix;
            return Path.Combine(Path.GetDirectoryName(plainFilePath), encryptedFileName);
        }
    }
}

