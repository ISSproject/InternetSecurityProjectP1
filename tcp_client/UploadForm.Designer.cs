﻿namespace tcp_client
{
    partial class UploadForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.button1 = new MetroFramework.Controls.MetroButton();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.metroTextButton1 = new MetroFramework.Controls.MetroTextBox.MetroTextButton();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.LoggedUserLabel = new MetroFramework.Controls.MetroLink();
            this.LoggedAsLabel = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(12, 132);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(369, 264);
            this.listBox1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 91);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(118, 35);
            this.button1.TabIndex = 1;
            this.button1.Text = "Browse Files";
            this.button1.UseSelectable = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // metroButton1
            // 
            this.metroButton1.Location = new System.Drawing.Point(263, 91);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(118, 35);
            this.metroButton1.TabIndex = 4;
            this.metroButton1.Text = "Clear List";
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click);
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.Font = new System.Drawing.Font("Tahoma", 14F);
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Location = new System.Drawing.Point(409, 167);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(217, 224);
            this.checkedListBox1.TabIndex = 6;
            // 
            // metroTextButton1
            // 
            this.metroTextButton1.Image = null;
            this.metroTextButton1.Location = new System.Drawing.Point(62, 408);
            this.metroTextButton1.Name = "metroTextButton1";
            this.metroTextButton1.Size = new System.Drawing.Size(247, 50);
            this.metroTextButton1.TabIndex = 7;
            this.metroTextButton1.Text = "Start Upload";
            this.metroTextButton1.UseSelectable = true;
            this.metroTextButton1.UseVisualStyleBackColor = true;
            this.metroTextButton1.Click += new System.EventHandler(this.metroTextButton1_Click);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel1.Location = new System.Drawing.Point(409, 132);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(142, 19);
            this.metroLabel1.TabIndex = 8;
            this.metroLabel1.Text = "Choose Allowed User:";
            // 
            // LoggedUserLabel
            // 
            this.LoggedUserLabel.Location = new System.Drawing.Point(567, 442);
            this.LoggedUserLabel.Name = "LoggedUserLabel";
            this.LoggedUserLabel.Size = new System.Drawing.Size(75, 23);
            this.LoggedUserLabel.TabIndex = 9;
            this.LoggedUserLabel.Text = "UserID";
            this.LoggedUserLabel.UseSelectable = true;
            // 
            // LoggedAsLabel
            // 
            this.LoggedAsLabel.AutoSize = true;
            this.LoggedAsLabel.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.LoggedAsLabel.Location = new System.Drawing.Point(495, 442);
            this.LoggedAsLabel.Name = "LoggedAsLabel";
            this.LoggedAsLabel.Size = new System.Drawing.Size(77, 19);
            this.LoggedAsLabel.TabIndex = 10;
            this.LoggedAsLabel.Text = "Logged As:";
            // 
            // UploadForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(665, 481);
            this.Controls.Add(this.LoggedAsLabel);
            this.Controls.Add(this.LoggedUserLabel);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.metroTextButton1);
            this.Controls.Add(this.checkedListBox1);
            this.Controls.Add(this.metroButton1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.listBox1);
            this.Name = "UploadForm";
            this.Text = "Upload Form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBox1;
        private MetroFramework.Controls.MetroButton button1;
        private MetroFramework.Controls.MetroButton metroButton1;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private MetroFramework.Controls.MetroTextBox.MetroTextButton metroTextButton1;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLink LoggedUserLabel;
        private MetroFramework.Controls.MetroLabel LoggedAsLabel;
    }
}