﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tcp_client
{
    public partial class RegisterForm : MetroForm
    {
        public CClient client;
        public static RegisterForm rform = null;
        public RegisterForm()
        {
            InitializeComponent();
            rform = this;
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            Regex pattern = new Regex("[0-9a-zA-Z]{6,}");
            if (pattern.IsMatch(PassBox.Text))
            {
                client = CClient.GetInstance();
                if (PassBoxCheck.Text.Equals(PassBox.Text))
                {
                    client.Register(this.IDBox.Text.ToString(), this.PassBox.Text.ToString());
                }
                else
                {
                    this.MatchLabel.Visible = true;
                }
            }
            else
            {
                MessageBox.Show("Password Minimum 6 characters required");
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            LoginForm form1 = new LoginForm();
            form1.Show();
            this.Hide();
        }

        // Check for Enter input!
        private void CheckKeys(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                LoginButton_Click(sender, e);
                e.Handled = true;
            }
        }


    }
}
