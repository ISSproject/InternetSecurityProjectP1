﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tcp_client
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new LoginForm());

            //Application.Run(new WlcForm());
            //RandomModel r = new RandomModel();
            //Console.WriteLine(RandomModel.GetRand());


            /*const int keySize = 1024;
            string publicAndPrivateKey;
            string publicKey;

            RSACrypt.GenerateKeys(keySize, out publicKey, out publicAndPrivateKey);

            string text = "text for encryption";
            string encrypted = RSACrypt.Encrypt(text, keySize, publicKey);
            string decrypted = RSACrypt.Decrypt(encrypted, keySize, publicAndPrivateKey);
            Console.WriteLine("Encrypted: {0}", encrypted);
            Console.WriteLine();
            Console.WriteLine("Decrypted: {0}", decrypted);


            Console.ReadLine();*/

        }
    }
}
