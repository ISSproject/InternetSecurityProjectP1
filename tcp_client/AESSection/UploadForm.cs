﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tcp_client.AESSection
{
    public partial class UploadForm : MetroForm
    {
        CClient CurrentClient = CClient.GetInstance();
        DataClasses1DataContext dc = new DataClasses1DataContext();
        public UploadForm()
        {
            InitializeComponent();
            listBox1.SelectionMode = SelectionMode.MultiSimple;
            var users = dc.Users.ToList();
            foreach (var item in users)
            {
                if (item.UserName != CurrentClient.ID)
                    checkedListBox1.Items.Add(item.UserName);
            }
            this.LoggedUserLabel.Text = CurrentClient.ID.ToString();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog FBD = new OpenFileDialog();
            FBD.InitialDirectory = @"C:\upload\";
            FBD.Filter = "All files (*.*)|*.*";
            FBD.FilterIndex = 2;
            FBD.RestoreDirectory = true;
            FBD.Multiselect = true;
            if (FBD.ShowDialog() == DialogResult.OK)
            {
                //listBox1.Items.Clear();
                string[] files = FBD.FileNames;
                foreach (string file in files)
                {
                    listBox1.Items.Add(/*Path.GetFileName*/(file));
                }
            }
        }
        private void metroButton1_Click(object sender, EventArgs e)
        {
            var selectedItems = new ListBox.SelectedObjectCollection(listBox1);
            selectedItems = listBox1.SelectedItems;

            if (listBox1.SelectedIndex != -1)
            {
                for (int i = selectedItems.Count - 1; i >= 0; i--)
                    listBox1.Items.Remove(selectedItems[i]);
            }
        }
        private void metroTextButton1_Click(object sender, EventArgs e)
        {
            string accessableClient = "";
            int i = 0;
            foreach (object itemChecked in checkedListBox1.CheckedItems)
                accessableClient += ((i++ == 0) ? itemChecked.ToString() : "-" + itemChecked.ToString());

            CurrentClient.reqUpload(listBox1.Items.Count, accessableClient);
            // Generate randome 16  Advanced Encryption Standard
            var key = RandomModel.GetRand();
            //Console.WriteLine(key);
            foreach (string item in listBox1.Items)
                CurrentClient.uploadFileWithAES(item, key, accessableClient);

            MessageBox.Show(key);
            var opform = new RSASection.OperationSucceedForm();
            opform.Text = "Upload Succeed";
            opform.Show();
        }

    }
}

