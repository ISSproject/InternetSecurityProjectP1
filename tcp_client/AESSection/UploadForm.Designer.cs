﻿namespace tcp_client.AESSection
{
    partial class UploadForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LoggedAsLabel = new MetroFramework.Controls.MetroLabel();
            this.LoggedUserLabel = new MetroFramework.Controls.MetroLink();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.UploadButton = new MetroFramework.Controls.MetroTextBox.MetroTextButton();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.ClearButton = new MetroFramework.Controls.MetroButton();
            this.Browsebutton = new MetroFramework.Controls.MetroButton();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // LoggedAsLabel
            // 
            this.LoggedAsLabel.AutoSize = true;
            this.LoggedAsLabel.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.LoggedAsLabel.Location = new System.Drawing.Point(495, 438);
            this.LoggedAsLabel.Name = "LoggedAsLabel";
            this.LoggedAsLabel.Size = new System.Drawing.Size(77, 19);
            this.LoggedAsLabel.TabIndex = 18;
            this.LoggedAsLabel.Text = "Logged As:";
            // 
            // LoggedUserLabel
            // 
            this.LoggedUserLabel.Location = new System.Drawing.Point(567, 438);
            this.LoggedUserLabel.Name = "LoggedUserLabel";
            this.LoggedUserLabel.Size = new System.Drawing.Size(75, 23);
            this.LoggedUserLabel.TabIndex = 17;
            this.LoggedUserLabel.Text = "UserID";
            this.LoggedUserLabel.UseSelectable = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel1.Location = new System.Drawing.Point(409, 128);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(142, 19);
            this.metroLabel1.TabIndex = 16;
            this.metroLabel1.Text = "Choose Allowed User:";
            // 
            // UploadButton
            // 
            this.UploadButton.Image = null;
            this.UploadButton.Location = new System.Drawing.Point(62, 404);
            this.UploadButton.Name = "UploadButton";
            this.UploadButton.Size = new System.Drawing.Size(247, 50);
            this.UploadButton.TabIndex = 15;
            this.UploadButton.Text = "Start Upload";
            this.UploadButton.UseSelectable = true;
            this.UploadButton.UseVisualStyleBackColor = true;
            this.UploadButton.Click += new System.EventHandler(this.metroTextButton1_Click);
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.Font = new System.Drawing.Font("Tahoma", 14F);
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Location = new System.Drawing.Point(409, 163);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(217, 204);
            this.checkedListBox1.TabIndex = 14;
            // 
            // ClearButton
            // 
            this.ClearButton.Location = new System.Drawing.Point(263, 87);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(118, 35);
            this.ClearButton.TabIndex = 13;
            this.ClearButton.Text = "Clear List";
            this.ClearButton.UseSelectable = true;
            this.ClearButton.Click += new System.EventHandler(this.metroButton1_Click);
            // 
            // Browsebutton
            // 
            this.Browsebutton.Location = new System.Drawing.Point(12, 87);
            this.Browsebutton.Name = "Browsebutton";
            this.Browsebutton.Size = new System.Drawing.Size(118, 35);
            this.Browsebutton.TabIndex = 12;
            this.Browsebutton.Text = "Browse Files";
            this.Browsebutton.UseSelectable = true;
            this.Browsebutton.Click += new System.EventHandler(this.button1_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(12, 128);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(369, 264);
            this.listBox1.TabIndex = 11;
            // 
            // UploadForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(665, 481);
            this.Controls.Add(this.LoggedAsLabel);
            this.Controls.Add(this.LoggedUserLabel);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.UploadButton);
            this.Controls.Add(this.checkedListBox1);
            this.Controls.Add(this.ClearButton);
            this.Controls.Add(this.Browsebutton);
            this.Controls.Add(this.listBox1);
            this.Name = "UploadForm";
            this.Text = "Upload Form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel LoggedAsLabel;
        private MetroFramework.Controls.MetroLink LoggedUserLabel;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox.MetroTextButton UploadButton;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private MetroFramework.Controls.MetroButton ClearButton;
        private MetroFramework.Controls.MetroButton Browsebutton;
        private System.Windows.Forms.ListBox listBox1;
    }
}