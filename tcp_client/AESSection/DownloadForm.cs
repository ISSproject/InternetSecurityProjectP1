﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tcp_client.AESSection
{
    public partial class DownloadForm : MetroForm
    {
        CClient CurrentClient = CClient.GetInstance();
        DataClasses1DataContext dc = new DataClasses1DataContext();
        string allFiles = "";
        public string privateKey = "";
        int error = 0;
        //public string kkey = "";

        private EventWaitHandle freeEvent = new EventWaitHandle(true, EventResetMode.ManualReset);
        public DownloadForm()
        {
            InitializeComponent();
            //listBox1.SelectionMode = SelectionMode.MultiSimple;
            allFiles = CurrentClient.reqDownload("AES");
            if (allFiles != null && allFiles != "")
            {
                string[] all = allFiles.Split('-');
                var items = listBox1.Items;
                foreach (string f in all)
                    items.Add(f);
            }
            LogTextBox.Multiline = true;
            LogTextBox.ScrollBars = RichTextBoxScrollBars.Vertical;
            LogTextBox.ReadOnly = true;
            LogTextBox.WordWrap = true;
        }
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var file = dc.Files.FirstOrDefault(c => c.Name.Equals(listBox1.SelectedItem));
            var filelog = dc.LogFiles.Where(c => c.FileId.Equals(file.Id)).ToList();
            LogTextBox.Text = "\t\tCreated By:  " + file.Owner.ToString() + "\n";
            foreach (var item in filelog)
            {
                LogTextBox.Text += "Updated By:  " + item.EditedBy.ToString() + "\n";
                LogTextBox.Text += "At:  " + item.EditDate.ToString() + "\n";
                LogTextBox.Text += "-------\n";
            }
        }
        private void DownloadButton_Click(object sender, EventArgs e)
        {
            if (!freeEvent.WaitOne(0))
            {
                MessageBox.Show("Please Wait Until Backend decryption Finish :)");
                return;
            }
            var selectedItems = new ListBox.SelectedObjectCollection(listBox1);
            selectedItems = listBox1.SelectedItems;
            List<string> dfiles = new List<string>();
            foreach (object itemChecked in selectedItems)
            {
                dfiles.Add(itemChecked.ToString());
            }
            if (dfiles.Count != 0)
            {
                var t = Task.Factory.StartNew(() =>
                {
                    freeEvent.Reset();
                    try
                    {
                        CurrentClient.downloadFileWithAES(dfiles, privateKey);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Please make sure you selected Your private Key !!");
                        error = 1;
                        this.Hide();
                    }
                    freeEvent.Set();
                });
            }
            if (error == 0)
            {
                var opform = new RSASection.OperationSucceedForm();
                opform.Text = "Download Succeed";
                opform.Show();
            }
        }
    }
}
