﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tcp_client
{
    public partial class DownloadSucceedForm : MetroForm
    {
        public DownloadSucceedForm()
        {
            InitializeComponent();
        }

        private void metroTextButton1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
